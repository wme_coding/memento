package state;

public class State {
    private String stateName;
    //Другие поля, которые описывают конечный автомат


    public State(String stateName) {
        this.stateName = stateName;
    }

    public String getStateName() {
        return stateName;
    }
}
