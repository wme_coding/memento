import org.junit.Test;
import state.State;

public class TestService {

    @Test
    public void testService(){
        FiniteStateMachine finiteStateMachine = new FiniteStateMachine();
        CareTaker careTaker = new CareTaker();

        finiteStateMachine.setState(new State("State #1"));
        finiteStateMachine.setState(new State("State #2"));
        careTaker.add(finiteStateMachine.saveStateToMemento());

        finiteStateMachine.setState(new State("State #3"));
        careTaker.add(finiteStateMachine.saveStateToMemento());

        finiteStateMachine.setState(new State("State #4"));
        System.out.println("Current State: " + finiteStateMachine.getState().getStateName());

        finiteStateMachine.getStateFromMemento(careTaker.get(0));
        System.out.println("First saved State: " + finiteStateMachine.getState().getStateName());
        finiteStateMachine.getStateFromMemento(careTaker.get(1));
        System.out.println("Second saved State: " + finiteStateMachine.getState().getStateName());
    }
}
